Hello Kevin,

I've pulled the receipts data into snowflake and done some basic analysis. While looking at some of our top purchases by brand, I found what I believe to be some missing data in the brands source. 

We have a lot of purchased items with barcodes that:
  * Have a partner ID associated with them and
  * Do not have a barcode lookup in our Brands data.  

Do you know if we have a more complete Brands file available? If not, I think we should look at integrating some additional partner data into this data source so that we have a more reliable lookup on items.

We also seem to have test data alongside production data for all of our data sources. Should I look at filtering or flagging this data on import?

I have some additional items around long term use of the data that I would like your input on:
  * Our Users data has a lot of duplicates. If you are comfortable with it, I will clean up duplicates so we can join data cleanly with their receipts.
  * Keeping the receipts and items data model that I have now will start to get expensive at scale. Do you have any predefined use cases you can provide me for how the data will be used? This will allow me to scale out the data in storage so we don't have to hit the warehouse so hard for frequent queries.

I'm happy to set something up to discuss.  
Thanks!  
Kevin Wiskow