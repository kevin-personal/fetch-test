Analytics Engineer - Fetch Rewards Coding Exercise  
Programmer: Kevin Wiskow

Requirements:
  * Review unstructured JSON data and diagram a new structured relational data model
  * Generate a query that answers a predetermined business question
  * Generate a query to capture data quality issues against the new structured relational data model
  * Write a short email or Slack message to the business stakeholder

SQL is written in snowflake against tables with the data. Variant/JSON data is largely
flattened to allow for analysis.

Contains:
  * 3 JSON files to upload to snowflake.
  * SQL_Transforms independent of the four questions.
  * Q1 through Q4 as separate files in the base folder.

I added a Calendar table to my data model, but I don't typically define hard relationships to it. There aren't performance
reasons for most tools, and its implied for most tables. I did not add a numbers table since it was not needed for this exercise.

Documentation of my thought process:
  * Dollar signs in JSON are a huge pain.
  * Users are duplicated heavily. This probably should be unique user per time period, but there are not issues with distincting it for now.
  * Receipts contains a list of items per receipt. This was split out to a separate table to join back on ReceiptID
  * Brands has a sub COG type object. I pulled out the ID to put on the base table, as it seems to join to Receipt_Items
  * I've done a lot of switching to Fact/Dim tables previously, but I'm finding with snowflake that wide tables are generally more performant than joining. This does introduce issues with some BI tools, and can create more cost if dimensions are queried regularly.
  * Fact -> fact tables are also not supported in many BI tools, so Receipt_Items and Receipts would potentially need to be retooled into more of a dim/fact setup with Receipt_Items not requiring a lookup to Receipts.

For the reviewer, I have placed each question into its own file with Q# at the front. Answers are commented in the files in addition to other observations.

Thanks!
Kevin Wiskow