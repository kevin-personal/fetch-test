--Second Question: SQL Query to answer questions.

--When considering average spend from receipts with 'rewardsReceiptStatus’ of ‘Accepted’ or ‘Rejected’, which is greater?
select rewardsreceiptstatus, avg(totalspent) as Avg_Total_Spent
from public.receipt_fact 
group by rewardsreceiptstatus
;
--There's no Accepted column. If the category is Finished, it is greater than Rejected.


--When considering total number of items purchased from receipts with 'rewardsReceiptStatus’ of ‘Accepted’ or ‘Rejected’, which is greater?
with agg_items as 
    (
        select rewardsreceiptstatus
        , sum(purchaseditemcount) as Sum_Item_Count
        from public.receipt_fact 
        where REWARDSRECEIPTSTATUS IN ('FINISHED', 'REJECTED')
        group by rewardsreceiptstatus
    )
select 
rewardsreceiptstatus
, Sum_Item_Count
from agg_items
--Return only the item with the highest Sum_Item_Count
qualify row_number() over (order by Sum_Item_Count desc) = 1
;
--Same as above. Finished is significantly more than Rejected.


--What are the top 5 brands by receipts scanned for the most recent month?
--Potentail data issue. There are a lot of barcdoes from receipt_items without a brand lookup
--, and COG -> REWARDSPRODUCTPARTNERID is many brands to COG_ID.
select distinct 
rec_item.RECEIPTID
, rec_item.BARCODE
, brand.BRANDNAME
, brand.COG_ID
, brand.BARCODE
from RECEIPT_ITEM_FACT rec_item
join BRAND_LUP brand
--    on rec_item_fact.REWARDSPRODUCTPARTNERID = brand.COG_ID
    ON rec_item.BARCODE = brand.BARCODE
    ;

--Additionally there is no data for March. If it is for January, I find Tostitos with 23 items.
select  
rec_item.RECEIPTID
, rec_item.barcode
, brand.BRANDNAME
, brand.COG_ID
, brand.BARCODE
, rec.DATESCANNED
from RECEIPT_ITEM_FACT rec_item
join RECEIPT_FACT rec
    on rec_item.RECEIPTID = rec.RECEIPTID
--Find the month prior to our current month so we have the complete most recent month.
join CALENDAR c
    on c.datekey = rec.DATESCANNED::date
    and c.month = month(dateadd(month, -1, rec.DATESCANNED))
join BRAND_LUP brand
--    on rec_item_fact.REWARDSPRODUCTPARTNERID = brand.COG_ID
    on rec_item.BARCODE = brand.BARCODE
    ;