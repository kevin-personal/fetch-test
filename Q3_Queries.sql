--Thoughts on quality issue. There are a significant number of items where the barcode is not associated with a brand,
--but the partner ID seems to be associated with partners. Could we be missing brand lookups, or is that handled another way?
with 
find_missing_brands as 
    (
        select 
        receipt_fact.description
        , receipt_fact.barcode
        , receipt_fact.rewardsproductpartnerid
        , count(*) as Missing_Item_Occurance
        from PUBLIC.RECEIPT_ITEM_FACT receipt_fact
        left join PUBLIC.BRAND_LUP brand
            on brand.barcode = receipt_fact.barcode
        where brand.barcode is null
        group by 
            receipt_fact.description
            , receipt_fact.barcode
            , receipt_fact.rewardsproductpartnerid
    )

select * from find_missing_brands
order by Missing_Item_Occurance desc
;
--produces a list of items with description, barcode, and the rewardsproductpartnerID.

