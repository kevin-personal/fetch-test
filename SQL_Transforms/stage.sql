--Program: Stage JSON objects
--Programmer: Kevin Wiskow
--Purpose: Stage JSON receipt, user, and brand objects for further analysis in SQL.
--uses SNOW SQL syntax.
   
    create or replace table USER_STAGE as

    select 
    get_path(c1, '_id:$oid')::string as userID
    ,to_timestamp(get_path(c1, 'createdDate:$date')::int, 3) as  createDate
    ,to_timestamp(get_path(c1, 'lastLogin:$date')::int, 3) as  lastLogin
    ,c1:role::string as user_role
    ,c1:active::boolean as active
    ,c1:signUpSource::string as signUpSource
    ,c1:state::string as userState
   from FETCHTEST.PUBLIC.USERSJSON users

   ;

   
   create or replace table RECEIPT_STAGE as
   
   select get_path(c1, '_id:$oid')::string as receiptID
   ,receipts.c1:bonusPointsEarned::int as bonusPointsEarned
   ,receipts.c1:bonusPointsEarnedReason::string as bonusPointsEarnedReason
   ,to_timestamp(get_path(c1, 'createDate:$date')::VARCHAR) as createDate
   ,to_timestamp(get_path(c1, 'dateScanned:$date')::VARCHAR) as dateScanned
   ,to_timestamp(get_path(c1, 'finishedDate:$date')::VARCHAR) as finishedDate
   ,to_timestamp(get_path(c1, 'modifyDate:$date')::VARCHAR) as modifyDate
   ,to_timestamp(get_path(c1, 'pointsAwardedDate:$date')::VARCHAR) as pointsAwardedDate
   ,to_timestamp(get_path(c1, 'purchaseDate:$date')::VARCHAR) as purchaseDate
   ,receipts.c1:pointsEarned::int as pointsEarned
   ,receipts.c1:purchasedItemCount::int as purchasedItemCount
   ,receipts.c1:rewardsReceiptStatus::string as rewardsReceiptStatus
   ,receipts.c1:totalSpent::double as totalSpent
   ,receipts.c1:userID::string as userID
   from receiptsjson receipts   
   ;
   
   
   create or replace table receipt_items_stage as
   
   select 
   get_path(c1, '_id:$oid')::string as receiptID
   
   ,itemList.value:barcode::string as barcode
   ,itemList.value:description::string as description
   ,itemList.value:finalPrice::double as finalPrice
   ,itemList.value:itemPrice::double as itemPrice
   ,itemList.value:needsFetchReview::string as needsFetchReview
   ,itemList.value:needsFetchReviewReason::string as needsFetchReviewReason
   ,itemList.value:partnerItemID::string as partnerItemID
   ,itemList.value:pointsNotAwardedReason::string as pointsNotAwardedReason
   ,itemList.value:pointsPayerID::string as pointsPayerID
   ,itemList.value:preventTargetGapPoints::boolean as preventTargetGapPoints
   ,itemlist.value:quantityPurchased::int as quantityPurchased
   ,itemList.value:rewardsGroup::string as rewardsGroup
   ,itemList.value:rewardsProductPartnerId::string as rewardsProductPartnerId
   ,itemList.value:userFlaggedBarcode::string as userFlaggedBarcode
   ,itemList.value:userFlaggedDescription::string as userFlaggedDescription
   ,itemList.value:userFlaggedNewItem::string as userFlaggedNewItem
   ,itemList.value:userFlaggedPrice::double as userFlaggedPrice
   ,itemList.value:userFlaggedQuantity::int as userFlaggedQuantity
   from receiptsjson receipts 
   ,lateral flatten( input => c1:rewardsReceiptItemList) itemlist
   ;
   
   
   
   create or replace table brand_stage as
   
   select 
   get_path(c1, '_id:$oid')::string as brandID
   ,c1:name::string as brandName
   ,c1:brandCode::string as brandCode
   ,c1:barcode::string as barcode
   ,c1:category::string as brandCategory
   ,c1:categoryCode::string as brandCategoryCode
   ,c1:topBrand::boolean as topBrand
   ,c1:cpg::variant as cpg
   from FETCHTEST.PUBLIC.BRANDSJSON
   ;
   