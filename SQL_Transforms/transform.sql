--Program: Create receipt data structures
--Programmer: Kevin Wiskow
/*Purpose: This creates tables that can be cleanly joined and aggregated. 
Receipts are split out from receipt items so that items can be rolled across.
Future improvements to this include making tables additive instead of replacements.
If there are additional hierarchies in the source JSON, tables will need to be added or changed
to support.
*/

--Brand Lookup creation
create or replace table BRAND_LUP as
select BRANDID
, BRANDNAME
, BRANDCODE
, BARCODE
, BRANDCATEGORY
, BRANDCATEGORYCODE
, TOPBRAND
, COG_ID
from FETCHTEST.PUBLIC.BRAND_STAGE
;

--Receipts without items
create or replace table RECEIPT_FACT as 
select 
RECEIPTID
, BONUSPOINTSEARNED
, CREATEDATE
, DATESCANNED
, FINISHEDDATE
, MODIFYDATE
, POINTSAWARDEDDATE
, PURCHASEDATE
, POINTSEARNED
, PURCHASEDITEMCOUNT
, REWARDSRECEIPTSTATUS
, TOTALSPENT
, USERID
from FETCHTEST.PUBLIC.RECEIPT_STAGE
;

--Receipt items. Does not include receipt base info.
create or replace table RECEIPT_ITEM_FACT as
select 
RECEIPTID
, BARCODE
, DESCRIPTION
, FINALPRICE
, ITEMPRICE
, NEEDSFETCHREVIEW
, NEEDSFETCHREVIEWREASON
, PARTNERITEMID
, POINTSNOTAWARDEDREASON
, POINTSPAYERID
, PREVENTTARGETGAPPOINTS
, QUANTITYPURCHASED
, REWARDSGROUP
, REWARDSPRODUCTPARTNERID
, USERFLAGGEDBARCODE
, USERFLAGGEDDESCRIPTION
, USERFLAGGEDNEWITEM
, USERFLAGGEDPRICE
, USERFLAGGEDQUANTITY
from FETCHTEST.PUBLIC.RECEIPT_ITEMS_STAGE
;

--Unique user. Will need to be windowed for future use.
create or replace table USER_LUP as
select distinct
USERID
,USER_ROLE
,ACTIVE
,SIGNUPSOURCE
,USERSTATE
from FETCHTEST.PUBLIC.USER_STAGE
;
